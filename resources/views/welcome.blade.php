		<!DOCTYPE html>
		<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<title>School Manager</title>

			<!-- Fonts -->
			<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
			<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

			<!-- Styles -->
			<style>
				html,
				body {
					background-color: #fff;
					color: #636b6f;
					font-family: 'Nunito', sans-serif;
					font-weight: 200;
					margin: 0;
				}

				.title {
					font-size: 62px;
					text-align: center;
				}

				.links>a {
					color: white;
					font-size: 18px;
					text-decoration: underline;
					font-weight: 600;
					letter-spacing: .1rem;
					text-transform: uppercase;
				}
			</style>
		</head>

		<body>
			<div class="title m-b-md">
				Administration de l'école
			</div>

			<div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
				<ol class="carousel-indicators">
					<li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
					<li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
					<li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="/img/Promotion.png" class="d-block w-100" id="image_carou" alt="...">
						<div class="carousel-caption d-none d-md-block">
							<div class="links">
								<a href="{{ route('promotions.index') }}"> Edition de la Promotion </a>
							</div>
						</div>
					</div>
					<div class="carousel-item">
						<img src="/img/student.jpg" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block">
							<div class="links">
								<a href="{{ route('students.index') }}"> Edition des Elèves </a>
							</div>
						</div>
					</div>
					<div class="carousel-item">
						<img src="/img/modules.png" class="d-block w-100" alt="...">
						<div class="carousel-caption d-none d-md-block">
							<div class="links">
								<a href="{{ route('modules.index') }}"> Edition des Modules </a>
							</div>
						</div>
					</div>
				</div>

				<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</a>
			</div>

			<div class="flex-center position-ref full-height">
				@if (Route::has('login'))
				<div class="top-right links">
					@auth
					<a href="{{ url('/home') }}">Home</a>
					@else
					<a href="{{ route('login') }}">Login</a>

					@if (Route::has('register'))
					<a href="{{ route('register') }}">Register</a>
					@endif
					@endauth
				</div>
				@endif

				<div class="content">


					<div class="links">
						<a href="{{ route('students.index') }}">Student Manager</a>
					</div>
				</div>
			</div>
		</body>

		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
		<script>
			src = "../js/app.js"
		</script>

		</html>