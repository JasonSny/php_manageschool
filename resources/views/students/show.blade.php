@extends('layouts.app')

@section('title')
Elèves Infos
@endsection

@section('content')

<style>
  .student_info {
    margin-left: 38%;
    margin-bottom: 3%;
  }

  .gg-menu-hotdog {
    margin-top: 8%;
    margin-left: 40%;
    margin-bottom: 13%;
  }


  .gg-trash {
    margin-top: 10%;
    margin-left: 45%;
    margin-bottom: 10%;
  }

  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
  }

  .tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
  }

  .tab button:hover {
    background-color: #ddd;
  }

  .tab button.active {
    background-color: #ccc;
  }

  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }

  #div_btn_student {
    margin-left: 40%;
    margin-top: 1%;
  }

  #btn_stu {
    margin-left: 39%;
    padding-left: 1%;
    width: 250px;
  }
</style>


<div class="column">
  <div class="student_info">
    <div class="row g-0">
      <div class="col-md-4 text-center" style="margin-top: 10%">
        <div class="card-body">
          <h5 class="card-title" style="font-size: 24px">{{ $student->name }} {{ $student->firstName }}</h5><br>
          <p class="card-text"> Adresse mail : {{ $student->email }}</p>
          <p class="card-text"><small class="text-muted"> Etudiant créer le : {{ $student->created_at }}</small></p>
          <p class="card-text"><small class="text-muted"> Mise à jour de l'étudiant le : {{ $student->updated_at }}</small></p>
        </div>
      </div>
    </div>
  </div>

  <div class="tab">
    <button class="tablinks" onclick="openCity(event, 'Promotion')"> Promotion </button>
    <button class="tablinks" onclick="openCity(event, 'Modules')"> Modules </button>
  </div>

  <div id="Promotion" class="tabcontent">
    @if (isset($student->promotion))
    <h5 class="card-title" style="font-size: 21px">{{ $student->promotion->name }} | {{ $student->promotion->speciality }}</h5><br>
    <p style="font-size: 14px"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rhoncus eget tellus
      id posuere. Nam suscipit eros suscipit mollis egestas. Etiam pharetra, mauris ut tincidunt faucibus,
      est neque fermentum dolor, id vulputate. </p>
    <a href="{{route('promotions.show', ['promotion'=>$student->promotion])}}" id="btn_stu" class="d-block btn btn-warning text-white"><i class="gg-info"></i></a>
    @endif
  </div>

  <div id="Modules" class="tabcontent">
    @if (isset($student->modules[0]))
    @endif
    <div class="row">
      @include('modules.parts.listModule', ['collection'=>$student->modules])
    </div>
  </div>

  <div id="div_btn_student" class="d-flex flex-row mb-3">
    <div style="margin-right: 2%">
      <a href="{{ route('students.edit', ['student' => $student]) }}" class="d-block btn btn-dark "><i class="gg-menu-hotdog"></i> Configurer </a>
    </div>
    <div>
      <form method="POST" action="{{route('students.destroy', ['student' => $student] )}}" style="margin-right: 10px">
        @method("DELETE")
        @csrf
        <button class="d-block btn btn-danger text-white"><i class="gg-trash"></i> Supprimer </button>
      </form>
    </div>
  </div>

  @endsection

  <script>
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
  </script>