@extends('layouts.app')

@section('title')
Edition de l'élève
@endsection

<style>
    h1 {
        text-align: center;
        text-decoration: underline;
    }

    #btn_add_student {
        margin-top: 2%;
        padding-left: 47%;
    }

    .btn_edit_student {
        margin-top: 5%;
    }

    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    .tab button:hover {
        background-color: #ddd;
    }

    .tab button.active {
        background-color: #ccc;
    }

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>


@section('content')

<h1> Edition de l'étudiant </h1>
<br>

<form method="POST" action="{{ route('students.update', ['student'=>$student]) }}">
    @method("PUT")
    @csrf

    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'etudiant')"> Etudiant </button>
    </div>

    <div id="etudiant" class="tabcontent">
        <div class="mb-3">
            <input type="text" class="form-control form-control-lg" name="name" placeholder="Nom de famille" required>
        </div>
        <div class="mb-3">
            <input type="text" class="form-control form-control-lg" name="firstName" placeholder="Prénom" required>
        </div>
        <div class="mb-3">
            <input type="email" class="form-control form-control-lg" name="email" placeholder="@" required>
        </div>

        <h4> Ajouter l'édutiant à la promotion :</h4>
        <br>
        <div class="row">
            @foreach ($promotions as $promotion)
            <div class="col-sm-4">
                <div class="mb-3 form-check">
                    <input type="radio" class="form-check-input" id="promotion-{{ $promotion->id }}" value="{{ $promotion->id }}" name="promotion" @if(isset($student->promotion) && $promotion->id == $student->promotion->id ) checked @endif
                    >
                    <label class="form-check-label" for="promotion-{{ $promotion->id }}">{{ $promotion->name }} {{ $promotion->speciality }}</label>
                </div>
            </div>
            @endforeach
        </div>
        <h4> Ajouter l'étudiant à un module : </h3>
            <br>
            <div class="row">
                @foreach ($modules as $module)
                <div class="col-sm-4">
                    <div class="mb-3 form-check">
                        <label class="form-check-label" for="module-{{ $module->id }}">{{ $module->name }}</label>
                        <input type="checkbox" class="form-check-input" id="module-{{ $module->id }}" value="{{ $module->id }}" name="modules[]" @foreach($student->modules as $possibleModule)
                        @if($possibleModule->id == $module->id) checked @endif
                        @endforeach
                        >
                    </div>
                </div>
                @endforeach
            </div>
    </div>

    <div id="btn_edit_student">
        <a class="d-block btn btn-dark text-white" href="{{ route('promotions.create') }}"> Valider l'édition </a>
    </div>
</form>

@endsection

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>