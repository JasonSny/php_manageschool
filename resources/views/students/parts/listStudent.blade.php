<style>
    #btn_info-warning {
        margin-left: 36%;
        margin-right: 35%
    }

    .gg-info {
        margin-left: 47%;
    }

    .gg-menu-hotdog {
        margin-left: 47%;
        margin-top: 2%;
        margin-bottom: 1%;
    }

    .gg-trash {
        margin-left: 48%;
        margin-top: 2%;
        margin-bottom: 1%;
    }
</style>

@foreach ($collection as $item)
<div class="col-xl-4 mb-5 text-center">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title"><b>{{ $item->name }}</b> {{ $item->firstName }}</h5>
            <p class="card-text">{{ $item->email }}</p>
            @if ($button == 'yes')
            <div class="row  ">
                <div class="col-12 mb-2 ">
                    <a href="{{ route('students.show', ['student' => $item]) }}" class="d-block btn btn-warning text-white"><i class="gg-info"></i></a>
                </div>
                <div class="col-12 mb-2 ">
                    <a href="{{ route('students.edit', ['student' => $item]) }}" class="d-block btn btn-dark text-white"><i class="gg-menu-hotdog"></i></a>
                </div>
                <div class="col-12">
                    <form class="d-grid" method="POST" action="{{route('students.destroy', ['student' => $item] )}}">
                        @method("DELETE")
                        @csrf
                        <button class="d-block btn btn-danger text-white">
                            <i class="gg-trash"></i></button>
                    </form>
                </div>
            </div>
            @else
            <a href="{{ route('students.show', ['student' => $item]) }}" id="btn_info-warning" class="d-block btn btn-warning text-white"><i class="gg-info"></i></a>
            @endif
        </div>
    </div>
</div>
@endforeach