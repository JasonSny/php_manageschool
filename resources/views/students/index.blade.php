@extends('layouts.app')

@section('title')
Liste des élèves
@endsection

<style>
    h1 {
        text-align: center;
        text-decoration: underline;
    }

    #btn_add_student {
        margin-top: 2%;
        padding-left: 47%;
    }
</style>

@section('content')

<h1> Liste des étudiants </h1>
<br>

<div class="row">

    @if($search)
    @include('students.parts.listStudent', ['collection'=>$studentsSearch, 'button'=> 'yes'])

    @else
    <h2 class="mb-5"> Nouveau(x) étudiant(s)</h2>
    @include('students.parts.listStudent', ['collection'=>$freestudents, 'button'=> 'yes'])

    @foreach ($promotions as $promotion)
    <h2 class="mb-5">{{ $promotion['promotionNameSpeciality'] }} :</h2>
    @include('students.parts.listStudent', ['collection'=>$promotion['student'], 'button'=> 'yes'])
    @endforeach

    @endif
</div>

<div id="btn_add_student">
    <button type="submit" class="d-block btn btn-dark text-white"> Ajouter un étudiant </button>
</div>

@endsection