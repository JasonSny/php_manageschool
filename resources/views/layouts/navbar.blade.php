<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <img src="/img/LOGO_YNOV.png">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" href="{{ route('promotions.index') }}"> Edition des promotions </a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{{ route('modules.index') }}"> Edition des modules </a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{{ route('students.index') }}"> Edition des élèves </a>
        </li>
      </ul>

      <form class="d-flex" style="margin-top: 10px">
        <input class="form-control me-2" name='search' type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-warning" type="submit"><i class="gg-search"></i></button>
      </form>
    </div>
</nav>