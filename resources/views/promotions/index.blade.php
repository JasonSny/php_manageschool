@extends('layouts.app')

@section('title')
Promotions
@endsection

@section('body-title')
Promotion des différentes filières
@endsection



@section('content')

<style>
    body {
        background-color: #ececec;
    }

    .column {
        margin-left: 35%;
    }

    #test {
        background-color: #d3d3d3;
    }

    .gg-info {
        margin-top: 7%;
        margin-left: 40%;
        margin-bottom: 3%;
    }

    .gg-menu-hotdog {
        margin-top: 8%;
        margin-left: 40%;
        margin-bottom: 10%;
    }

    .gg-trash {
        margin-top: 10%;
        margin-left: 45%;
        margin-bottom: 10%;
    }

    .btn_add_promo {
        margin-left: 30%;
    }
</style>

<div class="column">

    @foreach ( $promotions as $promotion)
    <div class="col-xl-4">
        <div id="test" class="card text-center" style="width: 25rem; margin-bottom: 10px; margin-top: 10px;">
            <div class="card-body">
                <h5 class="card-title mb-4">{{ $promotion->name }} - {{ $promotion->speciality }}</h5>
            </div>

            <div class="row">
                <div class="col-sm-4 ">
                    <a href="{{ route('promotions.show', ['promotion' => $promotion]) }}" class="d-block btn btn-warning text-white"><i class="gg-info"></i></a>
                </div>
                <div class="col-sm-4 ">
                    <a href="{{ route('promotions.edit', ['promotion' => $promotion]) }}" class="d-block btn btn-dark text-white"><i class="gg-menu-hotdog"></i></a>
                </div>
                <div class="col-sm-4 ">
                    <form class="d-grid" method="POST" action="{{route('promotions.destroy', ['promotion' => $promotion] )}}">
                        @method("DELETE")
                        @csrf
                        <button type="submit" class="d-block btn btn-danger text-white"><i class="gg-trash"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @endforeach
</div>

<div id="btn_add_promo">
    <a class="d-block btn btn-dark text-white" href="{{ route('promotions.create') }}"> Créer une Promotion </a>
</div>

@endsection