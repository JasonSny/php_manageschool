@extends('layouts.app')

@section('title')
Gestion de la promotion
@endsection

@section('body-title')
Gestion de la promotion
@endsection

<style>
    #btn_add_promo {
        margin-top: 2%;
        padding-left: 47%;
    }

    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    .tab button:hover {
        background-color: #ddd;
    }

    .tab button.active {
        background-color: #ccc;
    }

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>

@section('content')

<form method="POST" action="{{ route('promotions.update', ['promotion'=> $promotion]) }}">
    @method("PUT")
    @csrf

    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'niv_etude')"> Niveau d'étude </button>
        <button class="tablinks" onclick="openCity(event, 'Filiere')"> Filière </button>
        <button class="tablinks" onclick="openCity(event, 'Modules')">Modules</button>
        <button class="tablinks" onclick="openCity(event, 'eleves_Promo')"> Elèves de la promo </button>
        <button class="tablinks" onclick="openCity(event, 'eleves_Restant')"> Elèves sans promotion </button>
    </div>

    <div id="niv_etude" class="tabcontent">
        <div class="mb-3">
            <input type="text" class="form-control form-control-lg" name="name" placeholder="B1, B2, .." required>
        </div>
    </div>

    <div id="Filiere" class="tabcontent">
        <div class="mb-5">
            <input type="text" class="form-control form-control-lg" name="speciality" placeholder=" Informatique, Marketing, .." required>
        </div>
    </div>

    <div id="Modules" class="tabcontent">
        <div class="row">
            <div class="col-4">
                <div class="row">
                    @foreach($modules as $module)
                    <div class="col-5 mb-4 form-check" style="margin-left: 20px">
                        <input type="checkbox" class="form-check-input" id="module-{{ $module->id }}" value="{{ $module->id }}" name="modules[]" @foreach($promotion->modules as $possibleModule)
                        @if($possibleModule->id == $module->id) checked @endif
                        @endforeach
                        >
                        <label class="form-check-label" for="module-{{ $module->id }}">{{ $module->name }}</label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div id="eleves_Promo" class="tabcontent">
        <div class="col-4">
            <div class="row">
                @foreach($promotion->students as $student)
                <div class="col-5 mb-4 form-check" style="margin-left: 20px">
                    <input type="checkbox" class="form-check-input" id="student-{{ $student->id }}" value="{{ $student->id }}" name="students[]" checked>
                    <label class="form-check-label" for="student-{{ $student->id }}">{{ $student->name }} {{ $student->firstName }}</label>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="eleves_Restant" class="tabcontent">
        <div class="col-4">
            <div class="row">
                @foreach($freeStudents as $freeStudent)
                <div class="col-5 mb-4 form-check" style="margin-left: 20px">
                    <input type="checkbox" class="form-check-input" id="student-{{ $freeStudent->id }}" value="{{ $freeStudent->id }}" name="students[]">
                    <label class="form-check-label" for="student-{{ $freeStudent->id }}">{{ $freeStudent->name }} {{ $freeStudent->firstName }}</label>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div id="btn_add_promo">
        <button type="submit" class="d-block btn btn-dark text-white"> Valider </button>
    </div>

</form>


@endsection

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>