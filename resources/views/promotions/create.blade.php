@extends('layouts.app')

@section('title')
Création de la promotion
@endsection

@section('body-title')
Création de la promotion
@endsection

@section('content')

<style>
    #btn_add_promo {
        margin-top: 2%;
        padding-left: 47%;
    }

    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    .tab button:hover {
        background-color: #ddd;
    }

    .tab button.active {
        background-color: #ccc;
    }

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>

<form method="POST" action="{{ route('promotions.store') }}">
    @csrf

    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'niv_etude')"> Niveau d'étude </button>
        <button class="tablinks" onclick="openCity(event, 'Filiere')"> Filière </button>
        <button class="tablinks" onclick="openCity(event, 'Modules')">Modules</button>
        <button class="tablinks" onclick="openCity(event, 'Eleves')"> Elèves restant </button>
    </div>
    <div id="niv_etude" class="tabcontent">
        <div class="mb-3">
            <label for="name" style="font-size: 20px"> Niveau d'étude :</label>
            <input type="text" class="form-control form-control-lg" name="name" placeholder="B1, B2, .." required>
        </div>
    </div>

    <div id="Filiere" class="tabcontent">
        <div class="mb-3">
            <label for="speciality" style="font-size: 20px"> Filière :</label>
            <input type="text" class="form-control form-control-lg" name="speciality" placeholder="Informatique, CréaDesign, .." required>
        </div>
    </div>

    <div id="Modules" class="tabcontent">
        <div class="row">
            @foreach ($modules as $module)
            <div class="col-sm-4">
                <div class="mb-3 form-check">
                    <label class="form-check-label" for="module-{{ $module->id }}">{{ $module->name }}</label>
                    <input type="checkbox" class="form-check-input" id="module-{{ $module->id }}" value="{{ $module->id }}" name="modules[]">
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div id="Eleves" class="tabcontent">
        <div class="row">
            @foreach ($students as $student)
            <div class="col-sm-4">
                <div class="mb-3 form-check">
                    <label class="form-check-label" for="student-{{ $student->id }}">{{ $student->name }} {{ $student->firstName }}</label>
                    <input type="checkbox" class="form-check-input" id="student-{{ $student->id }}" value="{{ $student->id }}" name="students[]">
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div id="btn_add_promo">
        <button type="submit" class="d-block btn btn-dark text-white"> Valider </button>
    </div>
</form>

@endsection

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>