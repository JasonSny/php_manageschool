@extends('layouts.app')

@section('title')
Informations Promotion
@endsection

@section('content')

<style>
    .gg-menu-hotdog {
        margin-top: 8%;
        margin-left: 40%;
        margin-bottom: 10%;
    }

    .gg-trash {
        margin-top: 10%;
        margin-left: 45%;
        margin-bottom: 10%;
    }

    .img_Promo {
        position: absolute;
        padding-right: 80%;

    }

    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    .tab button:hover {
        background-color: #ddd;
    }

    .tab button.active {
        background-color: #ccc;
    }

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #div_btn_promo {
        margin-left: 40%;
        margin-top: 1%;
    }
</style>

<div class="column">
    <img src="/img/toulouse_promo.png" class="img_Promo" alt="Ynov">

    <div class="col-md-8 text-center" style="margin-left: 15%">
        <div class="card-body">
            <h3 class="card-title">{{ $promotion->name }} | {{ $promotion->speciality }}</h3><br>
            <p style="font-size: 16px"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rhoncus eget tellus
                id posuere. Nam suscipit eros suscipit mollis egestas. Etiam pharetra, mauris ut tincidunt faucibus,
                est neque fermentum dolor, id vulputate. </p>
        </div>
    </div>

    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'Modules')">Modules</button>
        <button class="tablinks" onclick="openCity(event, 'Eleves')"> Elèves </button>
    </div>

    <div id="Modules" class="tabcontent">
        @if (isset($promotion->modules[0]))
        @endif
        <div class="row" style="margin-top: 2%;">
            @include('modules.parts.listModule', ['collection'=>$promotion->modules])
        </div>
    </div>

    <div id="Eleves" class="tabcontent">
        @if (isset($promotion->students[0]))
        @endif
        <div class="row mb-4">
            @include('students.parts.listStudent', ['collection'=>$promotion->students, 'button'=> 'no'])
        </div>
    </div>


    <div id="div_btn_promo" class="d-flex flex-row mb-3">
        <div style="margin-right: 2%">
            <a href="{{ route('promotions.edit', ['promotion' => $promotion]) }}" class="d-block btn btn-dark "><i class="gg-menu-hotdog"></i> Configurer </a>
        </div>
        <div>
            <form method="POST" action="{{route('promotions.destroy', ['promotion' => $promotion] )}}" style="margin-right: 10px">
                @method("DELETE")
                @csrf
                <button type="submit" class="d-block btn btn-danger text-white"><i class="gg-trash"></i> Supprimer </button>
            </form>
        </div>
    </div>
</div>
@endsection

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>