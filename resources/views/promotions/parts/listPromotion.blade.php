<style>
    .gg-info {
        margin-left: 40%;
        margin-top: 1%;
    }

    #div_btn_promo_detail {
        margin-left: 40%;
        margin-right: 38%;
    }
</style>

@foreach ( $collection as $promotion)
<div class="col-sm-6  text-center">
    <div class="card mb-4">
        <div class="card-body" id="div_btn_promo_detail">
            <h5 class="card-title">{{ $promotion->name }}</h5>
            <p class="card-text" style="font-size: 18px">{{ $promotion->speciality }}</p>
            <a href="{{ route('promotions.show', ['promotion'=>$promotion]) }}" class="d-block btn btn-warning text-white"><i class="gg-info"></i></a>
        </div>
    </div>
</div>
@endforeach