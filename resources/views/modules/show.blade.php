@extends('layouts.app')

@section('title')
Infos Module
@endsection

@section('content')

<style>
    .gg-menu-hotdog {
        margin-top: 8%;
        margin-left: 40%;
        margin-bottom: 10%;
    }

    .gg-trash {
        margin-top: 10%;
        margin-left: 45%;
        margin-bottom: 10%;
    }

    .img_Promo {
        position: absolute;
        padding-right: 80%;

    }

    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    .tab button:hover {
        background-color: #ddd;
    }

    .tab button.active {
        background-color: #ccc;
    }

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }

    #div_btn_promo_detail {
        margin-left: 40%;
        margin-top: 1%;
    }
</style>

<div class="card text-center mb-3 " style="font-size: 18px">
    <div class="card-body mb-2">
        <h5 class="card-title mb-3" style="font-size: 30px">{{ $module->name }}</h5>
    </div>
    <div class="card-footer text-muted">
        <p style="font-size: 16px"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus rhoncus eget tellus
            id posuere. Nam suscipit eros suscipit mollis egestas. Etiam pharetra, mauris ut tincidunt faucibus,
            est neque fermentum dolor, id vulputate. </p>
    </div>
</div>

<div class="tab">
    <button class="tablinks" onclick="openCity(event, 'Promotion')"> Promotion </button>
    <button class="tablinks" onclick="openCity(event, 'Eleves')"> Elèves </button>
</div>

<div id="Promotion" class="tabcontent">
    @if (isset($module->promotions[0]))
    @endif
    <div class="row">
        @include('promotions.parts.listPromotion', ['collection'=>$module->promotions])
    </div>
</div>
<div id="Eleves" class="tabcontent">
    @if (isset($module->students[0]))
    @endif
    <div class="row mb-4">
        @include('students.parts.listStudent', ['collection'=>$module->students, 'button'=> 'no'])
    </div>
</div>

@endsection

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>