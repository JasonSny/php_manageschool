@extends('layouts.app')

@section('title')
Liste des modules
@endsection

<style>
    body {
        background-color: #ececec;
    }

    .column {
        margin-left: 35%;
    }

    #test {
        background-color: #d3d3d3;
    }

    .gg-info {
        margin-top: 7%;
        margin-left: 40%;
        margin-bottom: 3%;
    }

    .gg-menu-hotdog {
        margin-top: 8%;
        margin-left: 40%;
        margin-bottom: 10%;
    }

    .gg-trash {
        margin-top: 50%;
        margin-left: 45%;
        margin-bottom: 10%;
    }

    .btn_add_module {
        margin-left: 30%;
    }
</style>

@section('content')
<div class="column">
    @foreach ( $modules as $module)
    <div class="col-xl-4">
        <div id="test" class="card text-center" style="width: 25rem; margin-bottom: 10px; margin-top: 10px; ">
            <div class="card-body">
                <h5 class="card-title">{{ $module->name }}</h5>

                <div class="row">
                    <div class="col-4">
                        <a href="{{route('modules.show', ['module' => $module] )}}" class="d-block btn btn-warning text-white">
                            <i class="gg-info"></i>
                        </a>
                    </div>
                    <div class="col-4">
                        <a href="{{route('modules.edit', ['module' => $module] )}}" class="d-block btn btn-dark text-white">
                            <i class="gg-menu-hotdog"></i>
                        </a>
                    </div>
                    <div class="col-4">
                        <form class="d-grid" method="POST" action="{{route('modules.destroy', ['module' => $module] )}}">
                            @method("DELETE")
                            @csrf
                            <button type="submit" class="d-block btn btn-danger text-white">
                                <i class="gg-trash"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div id="btn_add_module">
    <a class="d-block btn btn-dark text-white" href="{{ route('modules.create') }}"> Créer un module </a>
</div>

@endsection