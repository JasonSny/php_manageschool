@extends('layouts.app')

@section('title')
Infos Module
@endsection

<style>
    #btn_add_modules {
        margin-top: 2%;
        padding-left: 47%;
    }

    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    .tab button:hover {
        background-color: #ddd;
    }

    .tab button.active {
        background-color: #ccc;
    }

    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>

@section('content')

<form method="POST" action="{{ route('modules.update', ['module' => $module]) }}">
    @method("PUT")
    @csrf

    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'module_Name')"> Nom du module </button>
        <button class="tablinks" onclick="openCity(event, 'list_promo_toModule')"> Liste des promotions </button>
    </div>

    <div id="module_Name" class="tabcontent">
        <div class="mb-3">
            <label for="name" style="font-size: 20px"> Nom de module :</label>
            <input type="text" class="form-control form-control-lg" name="name" value="{{$module->name}}" placeholder="Entrer a promotion's name" required>
        </div>
    </div>

    <div id="list_promo_toModule" class="tabcontent">
        <div class="row mb-3">
            @foreach($promotions as $promotion)
            <div class="col mb-4 form-check">
                <input type="checkbox" class="form-check-input" id="promotion-{{ $promotion->id }}" value="{{ $promotion->id }}" name="promotions[]" @foreach($module->promotions as $possiblePromotion)
                @if($possiblePromotion->id == $promotion->id) checked @endif
                @endforeach
                >
                <label class="form-check-label" for="promotion-{{ $promotion->id }}">{{ $promotion->name }} | {{ $promotion->speciality }}</label>
            </div>
            @endforeach
        </div>
    </div>

    <div id="btn_add_modules">
        <button type="submit" class="d-block btn btn-dark text-white"> Valider </button>
    </div>
</form>

@endsection

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>