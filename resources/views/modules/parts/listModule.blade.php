<style>
    .gg-info {
        margin-top: 7%;
        margin-left: 40%;
        margin-bottom: 3%;
    }
    
</style>

@foreach ( $collection as $module)
    <div class="col-sm-2  text-center" >
        <div class="card mb-4">
            <div class="card-body">
                <h5 class="card-title">{{ $module->name }}</h5>
                <a href="{{route('modules.show', ['module' => $module] )}}" class="d-block btn btn-warning text-white"><i class="gg-info"></i></a>
            </div>
        </div>
    </div>
@endforeach